this is my labs of gnome shell light theme. the main experimentation located [here](https://gitlab.gnome.org/dikasp2/gnome-shell-light-theme-experimentation)

## WITH COMMUNITY EXTENSION

<img width="300" src=/assets/a.jpg>
<img width="300" src=/assets/aa.jpg>
<img width="300" src=/assets/b.jpg>
<img width="300" src=/assets/c.jpg>
<img width="300" src=/assets/d.png>

## BACKGROUND COMPABILITY TEST

### light background

<img width="300" src="/assets/l1.png">
<img width="300" src="/assets/l2.png">

### medium background

<img width="300" src="/assets/l4.png">
<img width="300" src="/assets/l3.png">

### dark background

<img width="300" src="/assets/l5.png">
<img width="300" src="/assets/l6.png">

## COLORFUL WORKSPACE TEST

<img width="300" src="/assets/a01.png">
<img width="300" src="/assets/a02.png">
<img width="300" src="/assets/44.png">
<img width="300" src="/assets/55.png">
<img width="300" src="/assets/33.png">
<img width="300" src="/assets/33b.png">

## WEEBS TEST

<img width="300" src=/assets/CC.png>
<img width="300" src=/assets/C.png>

## ARTWORK

<img width="300" src="/assets/66.png">
<img width="300" src="/assets/77.png">
<img width="300" src="/assets/99.png">
<img width="300" src="/assets/110.png">
<img width="300" src="/assets/88.png">

## BANNER

<img width="600" src=/assets/jediisgnomebig.png>

